#include <iostream>
#include "Random.h"
using namespace std;

enum Plane_status {null, arriving, departing};

class Plane {
public:
   Plane();
   Plane(int flt, int time, Plane_status status);
   void refuse() const;
   void land(int time) const;
   void fly(int time) const;
   int started() const;
   int flt_num;
   int clock_start;
   Plane_status state;
   int current_fuel;
};
