#include "Runway.h"

Runway::Runway(int limit)
/*
Post:  The Runway data members are initialized to record no
       prior Runway use and to record the limit on queue sizes.
*/

{
   queue_limit = limit;
   num_land_requests = num_takeoff_requests = 0;
   num_landings = num_takeoffs = 0;
   num_land_refused = num_takeoff_refused = 0;
   num_land_accepted = num_takeoff_accepted = 0;
   land_wait = takeoff_wait = idle_time = 0;
   num_crashed = 0;
   num_emergincy_landing = 0;
}

Error_code Runway::can_land(const Plane &current)
/*
Post:  If possible, the Plane current is added to the
       landing Queue; otherwise, an Error_code of overflow is
       returned. The Runway statistics are updated.
Uses:  class Extended_queue.
*/

{
   Error_code result;
   if (landing.size() < queue_limit)
      result = landing.append(current);
   else
   result = fail;
   num_land_requests++;

   if (result != success)
      num_land_refused++;
   else
      num_land_accepted++;

   return result;
}

Error_code Runway::can_land_emergency(const Plane &current)
/*
Post:  Used for P5, when we need to make the planes whose 
       fuel is not enough to wait in the queue
Uses:  class Extended_queue.
*/

{
   Error_code result;
   if(current.current_fuel <= landing.size() && emergency_landing.size() < queue_limit) //if current fuel is not enough
      result = emergency_landing.append(current);                                       //put this plane in an emergency queue
   else {
      if (landing.size() < queue_limit)
         result = landing.append(current);
      else
      result = fail;
   }
   num_land_requests++;

   if (result != success)
      num_land_refused++;
   else
      num_land_accepted++;

   return result;
}


Error_code Runway::can_depart(const Plane &current)
/*
Post:  If possible, the Plane current is added to the
       takeoff Queue; otherwise, an Error_code of overflow is
       returned. The Runway statistics are updated.
Uses:  class Extended_queue.
*/

{
   Error_code result;
   if (takeoff.size() < queue_limit)
      result = takeoff.append(current);
   else
      result = fail;
   num_takeoff_requests++;
   if (result != success)
      num_takeoff_refused++;
   else
      num_takeoff_accepted++;

   return result;
}


Runway_activity Runway::activity(int time, Plane &moving)
/*
Post:  If the landing Queue has entries, its front
       Plane is copied to the parameter moving
       and a result  land is returned. Otherwise,
       if the takeoff Queue has entries, its front
       Plane is copied to the parameter moving
       and a result  takeoff is returned. Otherwise,
       idle is returned. Runway statistics are updated.
Uses:  class Extended_queue.
*/

{
   Runway_activity in_progress;
   if(!emergency_landing.empty()){           //check if there is any plane need an emergency landing first
      emergency_landing.retrieve(moving);
      land_wait +=time-moving.started();
      num_landings++;
      num_emergincy_landing++;
      in_progress = land;
      emergency_landing.serve();
   }
   else if (!landing.empty()) {              //then check if there is any plane waiting in the landing queue
      landing.retrieve(moving);
      land_wait += time - moving.started();
      num_landings++;
      in_progress = land;
      landing.serve();
   }
   else if (!takeoff.empty()) {              //then check if there is any plane waiting in the takeoff queue
      takeoff.retrieve(moving);
      takeoff_wait += time - moving.started();
      num_takeoffs++;
      in_progress = takeOff;
      takeoff.serve();
   }

   else {
      idle_time++;
      in_progress = idle;
   }
   return in_progress;
}

void Runway::run_idle()
/*
Post: The specified time is printed with a message that the runway is idle.
*/
{
   cout << "Runway is idle." << endl;
}

void Runway::fuel_consum()
/*
Pre:  Every plane waiting in the landing queue and 
      emergency queue has a value which shows its current fuel
Post: Reduce every plane's fuel by 1 time unit, if its fuel 
      goes 0, this plane crash
*/
{
   Plane temp_plane;
   if(!landing.empty()){
      int landing_queue_size = landing.size();
      for(int i=0; i<landing_queue_size; i++){
         landing.retrieve(temp_plane);
         temp_plane.current_fuel--;
         if(temp_plane.current_fuel < 1){
            cout << "Plane number " << temp_plane.flt_num << " crashed." << endl;
            num_crashed++;
         }
         else if(temp_plane.current_fuel <= landing_queue_size){
            emergency_landing.append(temp_plane);
         }
         else
            temp.append(temp_plane);
         landing.serve();
      }

      for(int i=0; i<temp.size(); i++){
         temp.retrieve(temp_plane);
         landing.append(temp_plane);
         temp.serve();
      }
   }
   temp.clear();
   int emergency_queue_size = emergency_landing.size();
   if(!emergency_landing.empty()){
      for(int i=0; i<emergency_queue_size; i++){
         emergency_landing.retrieve(temp_plane);
         temp_plane.current_fuel--;
         if(temp_plane.current_fuel < 1){
            cout << "Plane number " << temp_plane.flt_num << " crashed." << endl;
            num_crashed++;
            emergency_landing.serve();
         }
         else {
            emergency_landing.serve();
            temp.append(temp_plane);
         }
      }

      for(int i=0; i<temp.size(); i++){
         temp.retrieve(temp_plane);
         emergency_landing.append(temp_plane);
         temp.serve();
      }
   }
   temp.clear();
}

void Runway::shut_down(int time) const
/*
Post: Runway usage statistics are summarized and printed.
*/

{
   cout << "Simulation has concluded after " << time << " time units." << endl
        << "Total number of planes processed "
        << (num_land_requests + num_takeoff_requests) << endl
        << "Total number of planes asking to land "
        << num_land_requests << endl
        << "Total number of planes asking to take off "
        << num_takeoff_requests << endl
        << "Total number of planes accepted for landing "
        << num_land_accepted << endl
        << "Total number of planes accepted for takeoff "
        << num_takeoff_accepted << endl
        << "Total number of planes refused for landing "
        << num_land_refused << endl
        << "Total number of planes refused for takeoff "
        << num_takeoff_refused << endl
        << "Total number of planes that landed "
        << num_landings << endl
        << "Total number of planes that took off "
        << num_takeoffs << endl
        << "Total number of planes left in landing queue "
        << landing.size() << endl
        << "Total number of planes left in takeoff queue "
        << takeoff.size() << endl
        << "Total number of planes landed through emergincy landing "
        << num_emergincy_landing << endl
        << "Total number of planes which crashed while waiting in the queue "
        << num_crashed << endl;
   cout << "Percentage of time runway idle "
        << 100.0 * (( float ) idle_time) / (( float ) time) << "%" << endl;
   cout << "Average wait in landing queue "
        << (( float ) land_wait) / (( float ) num_landings) << " time units";
   cout << endl << "Average wait in takeoff queue "
        << (( float ) takeoff_wait) / (( float ) num_takeoffs)
        << " time units" << endl;
   cout << "Average observed rate of planes wanting to land "
        << (( float ) num_land_requests) / (( float ) time)
        << " per time unit" << endl;
   cout << "Average observed rate of planes wanting to take off "
        << (( float ) num_takeoff_requests) / (( float ) time)
        << " per time unit" << endl;
}


void initialize(int &end_time, int &queue_limit, double &arrival_rate, double &departure_rate)
/*
Pre:  The user specifies the number of time units in the simulation,
      the maximal queue sizes permitted,
      and the expected arrival and departure rates for the airport.
Post: The program prints instructions and initializes the parameters
      end_time, queue_limit, arrival_rate, and departure_rate to
      the specified values.
Uses: utility function user_says_yes
*/

{
   cout << "This program simulates an airport with only one runway." << endl
        << "One plane can land or depart in each unit of time." << endl;
   cout << "Up to what number of planes can be waiting to land "
        << "or take off at any time? " << flush;
   cin  >> queue_limit;

   cout << "How many units of time will the simulation run?" << flush;
   cin  >> end_time;

   bool acceptable;
   do {
      cout << "Expected number of arrivals per unit time?" << flush;
      cin  >> arrival_rate;
      cout << "Expected number of departures per unit time?" << flush;
      cin  >> departure_rate;
      if (arrival_rate < 0.0 || departure_rate < 0.0)
         cerr << "These rates must be nonnegative." << endl;
      else
         acceptable = true;

      if (acceptable && arrival_rate + departure_rate > 1.0)
         cerr << "Safety Warning: This airport will become saturated. " << endl;

   } while (!acceptable);
}

P2_Runway::P2_Runway(int limit)
/*
Post:  The Runway data members are initialized to record no
       prior Runway use and to record the limit on queue sizes.
*/

{
   queue_limit = limit;
   num_land_requests = num_takeoff_requests = 0;
   num_landings = num_takeoffs = 0;
   num_land_refused = num_takeoff_refused = 0;
   num_land_accepted = num_takeoff_accepted = 0;
   land_wait = idle_time_1 = idle_time_2 = 0;
}

Error_code P2_Runway::can_land(const Plane &current)
/*
Post:  If possible, the Plane current is added to the
       landing Queue; otherwise, an Error_code of overflow is
       returned. The Runway statistics are updated.
Uses:  class Extended_queue.
*/

{
   Error_code result;
   if (landing.size() < queue_limit)
      result = landing.append(current);
   else
      result = fail;
   num_land_requests++;

   if (result != success)
      num_land_refused++;
   else
      num_land_accepted++;

   return result;
}

Error_code P2_Runway::can_depart(const Plane &current)
/*
Post:  If possible, the Plane current is added to the
       takeoff Queue; otherwise, an Error_code of overflow is
       returned. The Runway statistics are updated.
Uses:  class Extended_queue.
*/

{
   Error_code result;
   if (takeoff.size() < queue_limit)
      result = takeoff.append(current);
   else
      result = fail;
   num_takeoff_requests++;
   if (result != success)
      num_takeoff_refused++;
   else
      num_takeoff_accepted++;

   return result;
}

Runway_activity P2_Runway::landing_runway_activity(int time, Plane &moving)
/*
Post:  if the landing queue is not empty, accept 1 plane to landing
Uses:  class Extended_queue.
*/

{
   Runway_activity in_progress;
   if (!landing.empty()) {
      landing.retrieve(moving);
      land_wait += time - moving.started();
      num_landings++;
      in_progress = land;
      landing.serve();
   }
   else {
      idle_time_1++;
      in_progress = idle;
   }
   return in_progress;
}

Runway_activity P2_Runway::takeoff_runway_activity(int time, Plane &moving)
/*
Post:  if the takeoff queue is not empty, send 1 plane to takeoff
Uses:  class Extended_queue.
*/

{
   Runway_activity in_progress;
   if (!takeoff.empty()) {
      takeoff.retrieve(moving);
      takeoff_wait += time - moving.started();
      num_takeoffs++;
      in_progress = takeOff;
      takeoff.serve();
   }
   else {
      idle_time_2++;
      in_progress = idle;
   }
   return in_progress;
}

void P2_Runway::landing_runway_idle()
/*
Post: state that landing runway is idle
*/
{
   cout << "Landing runway is idle." << endl;
}

void P2_Runway::takeoff_runway_idle()
/*
Post: state that takeoff runway is idle
*/
{
   cout << "Takeoff runway is idle." << endl;
}

void P2_Runway::shut_down(int time) const
/*
Post: Runway usage statistics are summarized and printed.
*/

{
   cout << "Simulation has concluded after " << time << " time units." << endl
        << "Total number of planes processed "
        << (num_land_requests + num_takeoff_requests) << endl
        << "Total number of planes asking to land "
        << num_land_requests << endl
        << "Total number of planes asking to take off "
        << num_takeoff_requests << endl
        << "Total number of planes accepted for landing "
        << num_land_accepted << endl
        << "Total number of planes accepted for takeoff "
        << num_takeoff_accepted << endl
        << "Total number of planes refused for landing "
        << num_land_refused << endl
        << "Total number of planes refused for takeoff "
        << num_takeoff_refused << endl
        << "Total number of planes that landed "
        << num_landings << endl
        << "Total number of planes that took off "
        << num_takeoffs << endl
        << "Total number of planes left in landing queue "
        << landing.size() << endl
        << "Total number of planes left in takeoff queue "
        << takeoff.size() << endl;
   cout << "Percentage of time landing runway idle "
        << 100.0 * (( float ) idle_time_1) / (( float ) time) << "%" << endl;
   cout << "Percentage of time takeoff runway idle "
        << 100.0 * (( float ) idle_time_2) / (( float ) time) << "%" << endl;
   cout << "Average wait in landing queue "
        << (( float ) land_wait) / (( float ) num_landings) << " time units" << endl;
   cout <<  "Average wait in takeoff queue "
        << (( float ) takeoff_wait) / (( float ) num_takeoffs)
        << " time units" << endl;
   cout << "Average observed rate of planes wanting to land "
        << (( float ) num_land_requests) / (( float ) time)
        << " per time unit" << endl;
   cout << "Average observed rate of planes wanting to take off "
        << (( float ) num_takeoff_requests) / (( float ) time)
        << " per time unit" << endl;
}

P3_Runway::P3_Runway(int limit)
/*
Post:  The Runway data members are initialized to record no
       prior Runway use and to record the limit on queue sizes.
*/

{
   queue_limit = limit;
   num_land_requests = num_takeoff_requests = 0;
   num_landings = num_takeoffs = 0;
   num_land_refused = num_takeoff_refused = 0;
   num_land_accepted = num_takeoff_accepted = 0;
   land_wait = takeoff_wait = idle_time_1 = idle_time_2 = 0;
}


Error_code P3_Runway::can_land(const Plane &current)
/*
Post: if there is a vacancy in the landing queue, 
      1 plane is accepted into the landing queue.
      if the landing queue is full, and 1 plane need to land, 
      it will be accepted into another queue waiting for the 
      help from takeoff queue.
      the help queue can only accept 1 plane
Uses:  class Extended_queue.
*/

{
   Error_code result;
   if (landing.size() < queue_limit)
      result = landing.append(current);
   else if(help_land.empty())
      result = help_land.append(current);
   else
      result = fail;
   num_land_requests++;

   if (result != success)
      num_land_refused++;
   else
      num_land_accepted++;

   return result;
}


Error_code P3_Runway::can_depart(const Plane &current)
/*
Post:  If possible, the Plane current is added to the
       takeoff Queue; otherwise, an Error_code of overflow is
       returned. The Runway statistics are updated.
Uses:  class Extended_queue.
*/

{
   Error_code result;
   if (takeoff.size() < queue_limit)
      result = takeoff.append(current);
   else
      result = fail;
   num_takeoff_requests++;
   if (result != success)
      num_takeoff_refused++;
   else
      num_takeoff_accepted++;

   return result;
}


Runway_activity P3_Runway::landing_runway_activity(int time, Plane &moving)
/*
Post: landing runway server the waiting queue of landing in a priority
      if the landing queue is empty, landing runway could help for takeoff
Uses:  class Extended_queue.
*/

{
   Runway_activity in_progress;
   if (!landing.empty()) {
      landing.retrieve(moving);
      land_wait += time - moving.started();
      num_landings++;
      in_progress = land;
      landing.serve();
   } 
   else if(!takeoff.empty()){
      takeoff.retrieve(moving);
      takeoff_wait += time - moving.started();
      num_takeoffs++;
      in_progress = takeoff_from_land;
      takeoff.serve();
   }
   else{
      idle_time_1++;
      in_progress = idle;
   }
   return in_progress;
}

Runway_activity P3_Runway::takeoff_runway_activity(int time, Plane &moving)
/*
Post: takeoff runway would help plane in the help landing queue first,
      in which the landing queue is full
      if the help landing queue is empty, takeoff runway do the takeoff
      task first.
      if the takeoff queue is empty, takeoff runway can help for landing
Uses:  class Extended_queue.
*/

{
   Runway_activity in_progress;

   if(!help_land.empty()){
      help_land.retrieve(moving);
      land_wait += time - moving.started();
      num_landings++;
      in_progress = land_in_takeoff;
   }
   else if (!takeoff.empty()) {
      takeoff.retrieve(moving);
      takeoff_wait += time - moving.started();
      num_takeoffs++;
      in_progress = takeOff;
      takeoff.serve();
   }
   else if (!landing.empty())
   {
      landing.retrieve(moving);
      land_wait += time - moving.started();
      num_landings++;
      in_progress = land_in_takeoff;
      landing.serve();
   }
   else {
      idle_time_2++;
      in_progress = idle;
   }
   return in_progress;
}
void P3_Runway::landing_runway_idle()
/*
Post: state that landing runway is idle
*/
{
   cout << "Landing runway is idle." << endl;
}


void P3_Runway::takeoff_runway_idle()
/*
Post: state that takeoff runway is idle
*/
{
   cout << "Take-off runway is idle." << endl;
}

void P3_Runway::shut_down(int time) const
/*
Post: Runway usage statistics are summarized and printed.
*/

{
   cout << "Simulation has concluded after " << time << " time units." << endl
        << "Total number of planes processed "
        << (num_land_requests + num_takeoff_requests) << endl
        << "Total number of planes asking to land "
        << num_land_requests << endl
        << "Total number of planes asking to take off "
        << num_takeoff_requests << endl
        << "Total number of planes accepted for landing "
        << num_land_accepted << endl
        << "Total number of planes accepted for takeoff "
        << num_takeoff_accepted << endl
        << "Total number of planes refused for landing "
        << num_land_refused << endl
        << "Total number of planes refused for takeoff "
        << num_takeoff_refused << endl
        << "Total number of planes that landed "
        << num_landings << endl
        << "Total number of planes that took off "
        << num_takeoffs << endl
        << "Total number of planes left in landing queue "
        << landing.size() << endl
        << "Total number of planes left in takeoff queue "
        << takeoff.size() << endl;
   cout << "Percentage of time landing runway idle "
        << 100.0 * (( float ) idle_time_1) / (( float ) time) << "%" << endl;
   cout << "Percentage of time takeoff runway idle "
        << 100.0 * (( float ) idle_time_2) / (( float ) time) << "%" << endl;
   cout << "Average wait in landing queue "
        << (( float ) land_wait) / (( float ) num_landings) << " time units" << endl;
   cout << "Average wait in takeoff queue "
        << (( float ) takeoff_wait) / (( float ) num_takeoffs)
        << " time units" << endl;
   cout << "Average observed rate of planes wanting to land "
        << (( float ) num_land_requests) / (( float ) time)
        << " per time unit" << endl;
   cout << "Average observed rate of planes wanting to take off "
        << (( float ) num_takeoff_requests) / (( float ) time)
        << " per time unit" << endl;
}

P4_Runway::P4_Runway(int limit)
/*
Post:  The Runway data members are initialized to record no
       prior Runway use and to record the limit on queue sizes.
*/

{
   queue_limit = limit;
   num_land_requests = num_takeoff_requests = 0;
   num_landings = num_takeoffs = 0;
   num_land_refused = num_takeoff_refused = 0;
   num_land_accepted = num_takeoff_accepted = 0;
   land_wait = idle_time_1 = idle_time_2 = idle_time_3 = 0;
}

Error_code P4_Runway::can_land(const Plane &current)
/*
Post:  If possible, the Plane current is added to the
       landing Queue; otherwise, an Error_code of overflow is
       returned. The Runway statistics are updated.
Uses:  class Extended_queue.
*/

{
   Error_code result;
   if (landing.size() < queue_limit)
      result = landing.append(current);
   else
      result = fail;
   num_land_requests++;

   if (result != success)
      num_land_refused++;
   else
      num_land_accepted++;

   return result;
}

Error_code P4_Runway::can_depart(const Plane &current)
/*
Post:  If possible, the Plane current is added to the
       takeoff Queue; otherwise, an Error_code of overflow is
       returned. The Runway statistics are updated.
Uses:  class Extended_queue.
*/

{
   Error_code result;
   if (takeoff.size() < queue_limit)
      result = takeoff.append(current);
   else
      result = fail;
   num_takeoff_requests++;
   if (result != success)
      num_takeoff_refused++;
   else
      num_takeoff_accepted++;

   return result;
}

Runway_activity P4_Runway::landing_runway_activity(int time, Plane &moving)
/*
Post:  if the landing queue is not empty, accept 1 plane to landing
Uses:  class Extended_queue.
*/

{
   Runway_activity in_progress;
   if (!landing.empty()) {
      landing.retrieve(moving);
      land_wait += time - moving.started();
      num_landings++;
      in_progress = land;
      landing.serve();
   }
   else {
      idle_time_1++;
      in_progress = idle;
   }
   return in_progress;
}

Runway_activity P4_Runway::takeoff_runway_activity(int time, Plane &moving)
/*
Post:  if the takeoff queue is not empty, send 1 plane to takeoff
Uses:  class Extended_queue.
*/

{
   Runway_activity in_progress;
   if (!takeoff.empty()) {
      takeoff.retrieve(moving);
      takeoff_wait += time - moving.started();
      num_takeoffs++;
      in_progress = takeOff;
      takeoff.serve();
   }
   else {
      idle_time_2++;
      in_progress = idle;
   }
   return in_progress;
}

Runway_activity P4_Runway::third_runway_activity(int time, Plane &moving)
/*
Post: first, check if there is any plane waiting in the landing queue,
      if yes, accept it to land.
      second, check if there is any plane waiting in the takeoff queue,
      if yes, send it to takeoff
Uses:  class Extended_queue.
*/

{
   Runway_activity in_progress;
   if(!landing.empty()){
      landing.retrieve(moving);
      land_wait += time - moving.started();
      num_landings++;
      in_progress = land;
      landing.serve();
   }
   else if (!takeoff.empty()) {
      takeoff.retrieve(moving);
      takeoff_wait += time - moving.started();
      num_takeoffs++;
      in_progress = takeOff;
      takeoff.serve();
   }
   else {
      idle_time_3++;
      in_progress = idle;
   }
   return in_progress;
}

void P4_Runway::landing_runway_idle()
/*
Post: state the landing runway is idle
*/
{
   cout << "Landing runway is idle." << endl;
}

void P4_Runway::takeoff_runway_idle()
/*
Post: state the takeoff runway is idle
*/
{
   cout << "Takeoff runway is idle." << endl;
}

void P4_Runway::third_runway_idle()
/*
Post: state the third runway is idle
*/
{
   cout << "The third runway is idle." << endl;
}

void P4_Runway::shut_down(int time) const
/*
Post: Runway usage statistics are summarized and printed.
*/

{
   cout << "Simulation has concluded after " << time << " time units." << endl
        << "Total number of planes processed "
        << (num_land_requests + num_takeoff_requests) << endl
        << "Total number of planes asking to land "
        << num_land_requests << endl
        << "Total number of planes asking to take off "
        << num_takeoff_requests << endl
        << "Total number of planes accepted for landing "
        << num_land_accepted << endl
        << "Total number of planes accepted for takeoff "
        << num_takeoff_accepted << endl
        << "Total number of planes refused for landing "
        << num_land_refused << endl
        << "Total number of planes refused for takeoff "
        << num_takeoff_refused << endl
        << "Total number of planes that landed "
        << num_landings << endl
        << "Total number of planes that took off "
        << num_takeoffs << endl
        << "Total number of planes left in landing queue "
        << landing.size() << endl
        << "Total number of planes left in takeoff queue "
        << takeoff.size() << endl;
   cout << "Percentage of time landing runway idle "
        << 100.0 * (( float ) idle_time_1) / (( float ) time) << "%" << endl;
   cout << "Percentage of time takeoff runway idle "
        << 100.0 * (( float ) idle_time_2) / (( float ) time) << "%" << endl;
   cout << "Percentage of time the third runway idle "
        << 100.0 * (( float ) idle_time_3) / (( float ) time) << "%" << endl;
   cout << "Average wait in landing queue "
        << (( float ) land_wait) / (( float ) num_landings) << " time units" << endl;
   cout <<  "Average wait in takeoff queue "
        << (( float ) takeoff_wait) / (( float ) num_takeoffs)
        << " time units" << endl;
   cout << "Average observed rate of planes wanting to land "
        << (( float ) num_land_requests) / (( float ) time)
        << " per time unit" << endl;
   cout << "Average observed rate of planes wanting to take off "
        << (( float ) num_takeoff_requests) / (( float ) time)
        << " per time unit" << endl;
}