#include <time.h>
#include <limits.h>
#include <math.h>
class Random {
public:
   Random(bool pseudo = true);
   double random_real();
   int random_integer(int low, int high);
   int poisson(double mean);
//    Declare random-number generation methods here.
 private:
   int reseed();                 //  Re-randomize the seed.
   int seed, multiplier, add_on; //  constants for use in arithmetic operations
};