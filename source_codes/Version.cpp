#include "Version.h"

void P1version()

{
   cout  << endl
         << endl
         << "Version 1 is running...." << endl
         << "Basic version of airport simulation from text" << endl;
   int end_time;            //  time to run simulation
   int queue_limit;         //  size of Runway queues
   int flight_number = 0;
   double arrival_rate, departure_rate;
   initialize(end_time, queue_limit, arrival_rate, departure_rate);
   Random variable;
   Runway small_airport(queue_limit);
   for (int current_time = 0; current_time < end_time; current_time++) { //  loop over time intervals
      int number_arrivals = variable.poisson(arrival_rate);  //  current arrival requests
      for (int i = 0; i < number_arrivals; i++) {
         Plane current_plane(flight_number++, current_time, arriving);
         if (small_airport.can_land(current_plane) != success)
            current_plane.refuse();
      }

      int number_departures= variable.poisson(departure_rate); //  current departure requests
      for (int j = 0; j < number_departures; j++) {
         Plane current_plane(flight_number++, current_time, departing);
         if (small_airport.can_depart(current_plane) != success)
            current_plane.refuse();
      }

      cout << "Time unit: " << current_time << endl;
      Plane moving_plane;
      switch (small_airport.activity(current_time, moving_plane)) {
        //  Let at most one Plane onto the Runway at current_time.
      case land:
         moving_plane.land(current_time);
         break;
      case takeOff:
         moving_plane.fly(current_time);
         break;
      case idle:
         small_airport.run_idle();
      default: break;
      }
   }
   
   small_airport.shut_down(end_time);
}

void P2version(){
   cout  << endl
         << endl
         << "Version 2 is running...." << endl
         << "Give the airport two runways,"<< endl 
         << "one always used for landings and one always used for takeoffs." << endl;
   int end_time;
   int queue_limit;
   int flight_number = 0;
   double arrival_rate, departure_rate;
   initialize(end_time, queue_limit, arrival_rate, departure_rate);
   Random variable;
   P2_Runway P2_airport(queue_limit);                                    // declare a P2_Runway class
   for (int current_time = 0; current_time < end_time; current_time++) {
      int number_arrivals = variable.poisson(arrival_rate);
      for (int i = 0; i < number_arrivals; i++) {
         Plane current_plane(flight_number++, current_time, arriving);
         if (P2_airport.can_land(current_plane) != success)
            current_plane.refuse();
      }

      int number_departures= variable.poisson(departure_rate);
      for (int j = 0; j < number_departures; j++) {
         Plane current_plane(flight_number++, current_time, departing);
         if (P2_airport.can_depart(current_plane) != success)
            current_plane.refuse();
      }
      cout << "Time unit: "<< current_time << endl;

      Plane moving_plane_1, moving_plane_2;                             //since we have 2 runway, we serve 2 planes in 1 time unit
      Runway_activity landing_runway_activity = P2_airport.landing_runway_activity(current_time, moving_plane_1),  //1 plane is served by landing runway
                      takeoff_runway_activity = P2_airport.takeoff_runway_activity(current_time, moving_plane_2);  //1 plane is served by takeoff runway
      switch (landing_runway_activity) {
      case land:
         moving_plane_1.land(current_time);
         break;
      case takeOff:
         moving_plane_1.fly(current_time);
         break;
      case land_in_takeoff:
         moving_plane_1.fly(current_time);
         cout << "This plane landed through take-off runway." << endl;
         break;
      case takeoff_from_land:
         moving_plane_1.fly(current_time);
         cout << "This plane took off through landing runway." << endl;
         break;
      case idle:
         P2_airport.landing_runway_idle();
      }

   switch (takeoff_runway_activity) {
      case land:
         moving_plane_2.land(current_time);
         break;
      case takeOff:
         moving_plane_2.fly(current_time);
         break;
      case land_in_takeoff:
         moving_plane_2.fly(current_time);
         cout << "This plane landed through take-off runway." << endl;
         break;
      case takeoff_from_land:
         moving_plane_2.fly(current_time);
         cout << "This plane took off through landing runway." << endl;
         break;
      case idle:
         P2_airport.takeoff_runway_idle();
      }
   }
   P2_airport.shut_down(end_time);
};

void P3version(){
   cout  << endl
         << endl
         << "Version 3 is running...." << endl
         << "give the airport two runways, " << endl 
         <<"one usually used for landings and one usually used for takeoffs." << endl 
         << " If one of the queues is empty, then both runways can be used for the other queue." << endl 
         << " Also, if the landing queue is full and another plane arrives to land, " << endl 
         << "then takeoffs will be stopped and both runways used to clear the backlog of landing planes."<< endl;
   int end_time;
   int queue_limit;
   int flight_number = 0;
   double arrival_rate, departure_rate;
   initialize(end_time, queue_limit, arrival_rate, departure_rate);
   Random variable;
   P3_Runway P3_airport(queue_limit);                                    // declare a P3_Runway class
   for (int current_time = 0; current_time < end_time; current_time++) {
      int number_arrivals = variable.poisson(arrival_rate);
      for (int i = 0; i < number_arrivals; i++) {
         Plane current_plane(flight_number++, current_time, arriving);
         if (P3_airport.can_land(current_plane) != success)
            current_plane.refuse();
      }

      int number_departures= variable.poisson(departure_rate);
      for (int j = 0; j < number_departures; j++) {
         Plane current_plane(flight_number++, current_time, departing);
         if (P3_airport.can_depart(current_plane) != success)
            current_plane.refuse();
      }

      cout << "Time unit: "<< current_time << endl;
      Plane moving_plane_1, moving_plane_2;
      Runway_activity landing_runway_activity = P3_airport.landing_runway_activity(current_time, moving_plane_1), 
                      takeoff_runway_activity = P3_airport.takeoff_runway_activity(current_time, moving_plane_2);
      switch (landing_runway_activity) {
      case land:
         moving_plane_1.land(current_time);
         break;
      case takeOff:
         moving_plane_1.fly(current_time);
         break;
      case land_in_takeoff:
         moving_plane_1.fly(current_time);
         cout << "This plane landed through take-off runway." << endl;
         break;
      case takeoff_from_land:
         moving_plane_1.fly(current_time);
         cout << "This plane took off through landing runway." << endl;
         break;
      case idle:
         P3_airport.landing_runway_idle();
      }

   switch (takeoff_runway_activity) {
      case land:
         moving_plane_2.land(current_time);
         break;
      case takeOff:
         moving_plane_2.fly(current_time);
         break;
      case land_in_takeoff:
         moving_plane_2.fly(current_time);
         cout << "This plane landed through take-off runway." << endl;
         break;
      case takeoff_from_land:
         moving_plane_2.fly(current_time);
         cout << "This plane took off through landing runway." << endl;
         break;
      case idle:
         P3_airport.takeoff_runway_idle();
      }
   }

   P3_airport.shut_down(end_time);
};
void P4version(){
   cout  << endl
         << endl
         << "Version 4 is running...." << endl
         << "give the airport three runways, " << endl 
         << "one always reserved for each of landing and takeoff " << endl 
         << "and the third used for landings unless the landing queue is empty, " << endl 
         << "in which case it can be used for takeoffs." << endl;
   int end_time;
   int queue_limit;
   int flight_number = 0;
   double arrival_rate, departure_rate;
   initialize(end_time, queue_limit, arrival_rate, departure_rate);
   Random variable;
   P4_Runway P4_airport(queue_limit);                                    // declare a P4_Runway class

   for (int current_time = 0; current_time < end_time; current_time++) {
      int number_arrivals = variable.poisson(arrival_rate);
      for (int i = 0; i < number_arrivals; i++) {
         Plane current_plane(flight_number++, current_time, arriving);
         if (P4_airport.can_land(current_plane) != success)
            current_plane.refuse();
      }

      int number_departures= variable.poisson(departure_rate);
      for (int j = 0; j < number_departures; j++) {
         Plane current_plane(flight_number++, current_time, departing);
         if (P4_airport.can_depart(current_plane) != success)
            current_plane.refuse();
      }

      cout << "Time unit: "<< current_time << endl;
      Plane moving_plane_1, moving_plane_2, moving_plane_3;  //since we have 3 runways, we serve 3 planes in 1 time unit
      Runway_activity landing_runway_activity = P4_airport.landing_runway_activity(current_time, moving_plane_1),   //1 plane is served by landing runway
                      takeoff_runway_activity = P4_airport.takeoff_runway_activity(current_time, moving_plane_2),   //1 plane is served by takeoff runway
                      third_runway_activity = P4_airport.third_runway_activity(current_time, moving_plane_3);       //1 plane is served by the third runway
      
      switch (landing_runway_activity) {
         case land:
            moving_plane_1.land(current_time);
         break;
         case idle:
            P4_airport.landing_runway_idle();
         default: break;
      }

      switch (takeoff_runway_activity) {
         case takeOff:
            moving_plane_2.fly(current_time);
            break;
         case idle:
            P4_airport.takeoff_runway_idle();
         default: break;
      }

      switch (third_runway_activity) {
         case land:
            moving_plane_3.land(current_time);
            break;
         case takeOff:
            moving_plane_3.fly(current_time);
            break;
         case idle:
            P4_airport.takeoff_runway_idle();
         default: break;
      }
   }

   P4_airport.shut_down(end_time);
};
void P5version(){
   cout  << endl
         << endl
         << "Version 5 is running...." << endl
         << " If the plane does not have enough fuel to wait in the queue, " << endl 
         << "it is allowed to land immediately." << endl;
   int end_time;
   int queue_limit;
   int flight_number = 0;
   double arrival_rate, departure_rate;
   initialize(end_time, queue_limit, arrival_rate, departure_rate);
   Random variable;
   Runway small_airport(queue_limit);
   for (int current_time = 0; current_time < end_time; current_time++) {
      int number_arrivals = variable.poisson(arrival_rate);
      for (int i = 0; i < number_arrivals; i++) {
         Plane current_plane(flight_number++, current_time, arriving);
         if (small_airport.can_land_emergency(current_plane) != success)  //use the emergency landing method to determine if accept this landing plane
            current_plane.refuse();
      }

      int number_departures= variable.poisson(departure_rate);
      for (int j = 0; j < number_departures; j++) {
         Plane current_plane(flight_number++, current_time, departing);
         if (small_airport.can_depart(current_plane) != success)
            current_plane.refuse();
      }

      cout << "Time unit: " << current_time << endl;
      Plane moving_plane;
      switch (small_airport.activity(current_time, moving_plane)) {
      case land:
         moving_plane.land(current_time);
         break;
      case takeOff:
         moving_plane.fly(current_time);
         break;
      case idle:
         small_airport.run_idle();
      default: break;
      }
      small_airport.fuel_consum();
   }
   small_airport.shut_down(end_time);
};