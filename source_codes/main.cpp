#include <iostream>
#include "Version.h"
using namespace std;

int main()     //  Airport simulation program
/*
Pre:  The user must supply the number of time intervals the simulation is to
      run, the expected number of planes arriving, the expected number
      of planes departing per time interval, and the
      maximum allowed size for runway queues.
Post: The program performs a random simulation of the airport, showing
      the status of the runway at each time interval, and prints out a
      summary of airport operation at the conclusion.
Uses: Classes Runway, Plane, Random and functions run_idle, initialize.
*/

{
   int version_choice = -1;
   bool valid_choice = false;
   while(!valid_choice){
      cout << "Enter a number 1~6:" << endl
         << "1.	Run P1 version of Airport simulator" << endl
         << "2.	Run P2 version of Airport simulator" << endl
         << "3.	Run P3 version of Airport simulator" << endl
         << "4.	Run P4 version of Airport simulator" << endl
         << "5.	Run P6 version of Airport simulator" << endl
         << "6.	Quit the program" << endl
         << "Your choice: ";
      cin >> version_choice;
      if(version_choice >0 && version_choice < 7){
         valid_choice = true;
      }else cout << "Invalid choice. Please enter a number 1~6." 
                 << endl
                 << endl
                 << endl;
   }
   switch (version_choice)
   {
   case 1:
      P1version();
      break;
   case 2:
      P2version();
      break;
   case 3:
      P3version();
      break;
   case 4:
      P4version();
      break;
   case 5:
      P5version();
      break;
   case 6:
      cout << "Goodbye!" << endl;
      break;
   default:
      break;
   }

   return 1;
}