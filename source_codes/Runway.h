#include <iostream>
#include "extended_queue.h"
#include "Utility.h"
#include <string.h>

using namespace std;

enum Runway_activity {idle, land, takeOff, land_in_takeoff, takeoff_from_land};

class Runway {
public:
   Runway(int limit);
   Error_code can_land(const Plane &current);
   Error_code can_depart(const Plane &current);
   Error_code can_land_emergency(const Plane &current);  
   Runway_activity activity(int time, Plane &moving);
   void shut_down(int time) const;
   void run_idle();
   void fuel_consum();                                   //method which could reduce the current by 1 time unit

private:
   Extended_queue landing;
   Extended_queue takeoff;
   Extended_queue temp;
   Extended_queue emergency_landing;                    //extra queue for planes whose fuel is less than the waiting time
   int queue_limit;
   int num_landings;             //  number of planes that have landed
   int num_takeoffs;             //  number of planes that have taken off
   int num_land_accepted;        //  number of planes queued to land
   int num_takeoff_accepted;     //  number of planes queued to take off
   int num_land_refused;         //  number of landing planes refused
   int num_takeoff_refused;      //  number of departing planes refused
   int land_wait;                //  total time of planes waiting to land
   int takeoff_wait;             //  total time of planes waiting to take off
   int idle_time;                //  total time runway is idle
   int num_land_requests;        //  number of planes asking to land
   int num_takeoff_requests;     //  number of planes asking to take off
   int num_crashed;              //  number of planes crashed while waiting in the queue
   int num_emergincy_landing;    //  number of planes which made an emergincy landing
};

void initialize(int &end_time, int &queue_limit, double &arrival_rate, double &departure_rate);

void run_idle();

class P2_Runway {
public:
   P2_Runway(int limit);
   Error_code can_land(const Plane &current);
   Error_code can_depart(const Plane &current);
   Runway_activity landing_runway_activity(int time, Plane &moving);   //activity which acts the landing runway role
   Runway_activity takeoff_runway_activity(int time, Plane &moving);   //activity which acts the takeoff runway role
   void shut_down(int time) const;
   void landing_runway_idle();                                         //landing runway run idle
   void takeoff_runway_idle();                                         //takeoff runway run idle

private:
   Extended_queue landing;
   Extended_queue takeoff;
   int queue_limit;
   int num_landings;
   int num_takeoffs;
   int num_land_accepted;
   int num_takeoff_accepted;
   int num_land_refused;
   int num_takeoff_refused;
   int land_wait;                
   int takeoff_wait;             
   int idle_time_1;              //  total time the landing runway is idle
   int idle_time_2;              //  total time the takeoff runway is idle
   int num_land_requests;
   int num_takeoff_requests;
};


class P3_Runway {
public:
   P3_Runway(int limit);
   Error_code can_land(const Plane &current);
   Error_code can_depart(const Plane &current);
   Runway_activity landing_runway_activity(int time, Plane &moving);   //activity which acts the landing runway role
   Runway_activity takeoff_runway_activity(int time, Plane &moving);   //activity which acts the takeoff runway role
   void landing_runway_idle();                                         //landing runway run idle
   void takeoff_runway_idle();                                         //takeoff runway run idle
   void shut_down(int time) const;
private:
   Extended_queue landing;
   Extended_queue takeoff;
   Extended_queue help_land;     //extra queue for plane need to help when landing queue is full
   int queue_limit;
   int num_landings;
   int num_takeoffs;
   int num_land_accepted;
   int num_takeoff_accepted;
   int num_land_refused;
   int num_takeoff_refused;
   int land_wait;
   int takeoff_wait;
   int idle_time_1;              //  total time runway is idle
   int idle_time_2;              //  total time runway is idle
   int num_land_requests;
   int num_takeoff_requests;
};

class P4_Runway {
public:
   P4_Runway(int limit);
   Error_code can_land(const Plane &current);
   Error_code can_depart(const Plane &current);
   Runway_activity landing_runway_activity(int time, Plane &moving);   //activity which acts the landing runway role
   Runway_activity takeoff_runway_activity(int time, Plane &moving);   //activity which acts the takeoff runway role
   Runway_activity third_runway_activity(int time, Plane &moving);     //activity which acts the third runway role
   void shut_down(int time) const;
   void landing_runway_idle();                                         //landing runway run idle
   void takeoff_runway_idle();                                         //takeoff runway run idle
   void third_runway_idle();                                           //third runway run idle

private:
   Extended_queue landing;
   Extended_queue takeoff;
   int queue_limit;
   int num_landings;
   int num_takeoffs;
   int num_land_accepted;
   int num_takeoff_accepted;
   int num_land_refused;
   int num_takeoff_refused;
   int land_wait;
   int takeoff_wait;
   int idle_time_1;                //  total time landing runway is idle
   int idle_time_2;                //  total time takeoff runway is idle
   int idle_time_3;                //  total time the third runway is idle
   int num_land_requests;
   int num_takeoff_requests;
};